const express = require("express");
const app = express();
const port = 3000;

app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

const data = [
  { nama: "lutfi", umur: "17" },
  { nama: "rama", umur: "21" },
  { nama: "ajri", umur: "22" },
];

//TODO: tambah data divisi sebanyak 5 buah
const divisions = [
  {
    id: 1,
    name: "IT Development",
    head: "Lutfi",
  },
  {
    id: 2,
    name: "",
    head: "Rama",
  },
  {
    id: 3,
    name: "",
    head: "Ajri",
  },
  {
    id: 4,
    name: "",
    head: "Abdil",
  },
  {
    id: 5,
    name: "",
    head: "Sultan",
  },
];

app.get("/divisi", (req, res) => {
  //TODO: tampilkan seluruh data divisi
  res.send(divisions);
});

app.get("/divisi/:id", (req, res) => {
  //TODO: tampilkan data divisi yang dicari (1 data)
    const { id } = req.params;
    const filter = divisions.filter((value) => value.id == req.params.id);
  res.send( filter );
});

app.post("/divisi/:id", (req, res) => {
  //TODO: ubah data divisi sesuai dari body
  const { id } = req.params;
  const { name, head } = req.body;

  divisions.map((value) => {
    // cari data berdasarkan params id
    if (value.id == id) {
      // ubah data name & data head menjadi dari data yang dari body
      value.name = name;
      value.head = head;
    }
  });

  res.send(divisions);
});

// POST bisa nerima data body dari client (postman / brwser)
app.post("/ubah/:keyword", (req, res) => {
  const { keyword } = req.params;
  const { ngaran, kolot } = req.body;

  data.map((value) => {
    // cari data berdasarkan params user
    if (value.nama == keyword) {
      // ubah data nama & data umur menjadi dari data yang dari body
      value.nama = ngaran;
      value.umur = kolot;
    }
  });

  res.send(data);
});

app.get("/", (req, res) => {
  res.send("Hello World!");
});

// GET hanya nerima param & query (yang tandatanya di URL)
app.get("/cari/:keyword", (req, res) => {
  const { keyword } = req.params;

  // masukin data olan
  const newData = { nama: "olan", umur: "29" };
  data.push(newData);
  //   data[3] = newData;
  //   data[data.length] = newData;

  //   data.map((value, index) => {
  //     console.log(value, index);
  //   });

  //   const filteredData = data.filter((value) => {
  //     if (value.nama == keyword) {
  //       return true;
  //     }
  //   });

  //   const filteredData = data.filter((value) => {
  //     return value.nama == keyword;
  //   });

  const filter = data.filter((value) => value.nama == req.params.keyword);
  res.send({ filter });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
